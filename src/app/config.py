# -*- coding: utf-8 -*-

# stdlib imports
import pathlib

# 3rd party lib imports

# own stuff


basedir = pathlib.Path(__file__).absolute().parent


# SQLALCHEMY_DATABASE_URI = "sqlite:///" + str(basedir / "app.db")
SQLALCHEMY_DATABASE_URI = "postgresql+psycopg2://john:doe@localhost/unknown"
