# -*- coding: utf-8 -*-

# stdlib imports

# 3rd party lib imports
import sqlalchemy as sa
import sqlalchemy.ext.declarative

# own stuff


Base = sqlalchemy.ext.declarative.declarative_base()


class User(Base):
    __tablename__ = "user"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    firstname = sa.Column(sa.String)
    lastname = sa.Column(sa.String)
    email = sa.Column(sa.String, unique=True)
    home_country = sa.Column(sa.String(2))

    def __repr__(self):
        return "User(firstname={}. lastname={}, email={})".format(
            self.firstname, self.lastname, self.email
        )


class Workout(Base):
    __tablename__ = "workout"

    id = sa.Column(sa.Integer, primary_key=True, autoincrement=True)
    user_id = sa.Column(sa.Integer, sa.ForeignKey("user.id"))
    workout_type = sa.Column(sa.Integer)
    start = sa.Column(sa.DateTime)
    end = sa.Column(sa.DateTime)

    def __repr__(self):
        return "Workout(user_id={}, workout_type={}, start={}, end={})".format(
            self.user_id, self.workout_type, self.start, self.end
        )
