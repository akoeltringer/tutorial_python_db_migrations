"""02 Add Workout Table

Revision ID: 97ef71bd5ef6
Revises: a904d522b063
Create Date: 2020-04-01 00:34:46.363616

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = "97ef71bd5ef6"
down_revision = "a904d522b063"
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        "workout",
        sa.Column("id", sa.Integer(), autoincrement=True, nullable=False),
        sa.Column("user_id", sa.Integer(), nullable=True),
        sa.Column("workout_type", sa.Integer(), nullable=True),
        sa.Column("start", sa.DateTime(), nullable=True),
        sa.Column("end", sa.DateTime(), nullable=True),
        sa.ForeignKeyConstraint(["user_id"], ["user.id"],),
        sa.PrimaryKeyConstraint("id"),
    )
    op.add_column("user", sa.Column("country", sa.String(length=2), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column("user", "country")
    op.drop_table("workout")
    # ### end Alembic commands ###
