"""03 rename country column

Revision ID: 9f86fbcfb60f
Revises: 97ef71bd5ef6
Create Date: 2020-04-01 21:07:17.948764

"""
from alembic import op


# revision identifiers, used by Alembic.
revision = "9f86fbcfb60f"
down_revision = "97ef71bd5ef6"
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("user", "country", new_column_name="home_country")


def downgrade():
    op.alter_column("user", "home_country", new_column_name="country")
